/* see if this is a modern Intel platform */
int verifyIntel() 
{
	const int MAX_LINE_LENGTH = 256;
	/* make sure this gets closed later */
	FILE *cpuinfo = fopen("/proc/cpuinfo", "r");
	
	if (cpuinfo == NULL)
	{
		/* if /proc/cpuinfo is inaccessible post error messages */
		perror("Error opening /proc/cpuinfo");
		system("dvnotifier -i -c -m \"dvcpufreq could not open /proc/cpuinfo\" &");
		return 1;
	}

	char line[MAX_LINE_LENGTH];
	int foundVendor = 0;
	while (fgets(line, MAX_LINE_LENGTH, cpuinfo))
	{
		if (strncmp(line, "vendor_id", strlen("vendor_id")) == 0)
		{
			char vendor[MAX_LINE_LENGTH];
			sscanf(line, "%*s%*s%s", vendor);
			
			if (strcmp(vendor, "GenuineIntel") == 0)
			{
				printf("dvcpufreq has detected an Intel CPU. Starting normally...\n");
			}
			
			else if (strcmp(vendor, "AuthenticAMD") == 0)
			{
				printf("This appears to be an AMD CPU. xmcpupower has only been tested on Intel 8th gen platforms.\n");
				system("dvnotifier -i -c -m \"This appears to be an AMD CPU. xmcpupower has only been tested on Intel 8th gen platforms.\" &");
			}
			
			else
			{
				printf("Could not determine CPU vendor. xmcpupower has only been tested on Intel 8th gen platforms.\n");
				system("dvnotifier -i -c -m \"Could not determine CPU vendor. xmcpupower has only been tested on Intel 8th gen platforms.\" &");
			}
			
			break;
		}
	}
	

	fclose(cpuinfo);
	return 0;
}
