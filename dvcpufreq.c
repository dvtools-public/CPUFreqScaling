/* Adjust CPU frequency scaling on (older) Intel CPUs */
/* Source code is a WIP and kinda ugly */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> /* access() */
#include <string.h>

#include <Xm/Xm.h> /* motif core */
#include <Xm/MwmUtil.h> /* mwm functions and decor */
#include <Xm/CascadeB.h> /* dropdown menus */
#include <Xm/MessageB.h>
#include <Xm/PushB.h> /* reset button */
#include <Xm/RowColumn.h> /* menu bar and radio box */
#include <Xm/Form.h>
#include <Xm/Scale.h> /* freq sliders */
#include <Xm/Frame.h>
#include <Xm/Label.h>
#include <Xm/LabelG.h>
#include <Xm/ToggleB.h> /* perf gov radio toggles */
#include <Xm/Separator.h>
#include <Xm/DialogS.h> /* help popup dialogs */

#include <X11/extensions/Xinerama.h> /* window centering */
#include <X11/xpm.h> /* icons */

#include "icon_data.h"
#include "callbacks.h"


/* fallback X11 resources */
static String fbxres[] = 
{
	/* some optional font settings */
	"*renderTable: xft",
	"*xft*fontType: FONT_IS_XFT",
	"*xft*fontName: Roboto",
	"*xft*fontSize: 11",
	"*xft*autohint: 1",
	"*xft*lcdfilter: lcddefault",
	"*xft*hintstyle: hintslight",
	"*xft*hinting: True",
	"*xft*antialias: 1",
	"*xft*rgba: rgb",
	"*xft*dpi: 96",
	
	"*aboutLabel*fontStyle: Bold",
	"*usageLabel*fontStyle: Bold",
	"*cpuIconLabel*fontStyle: Bold Italic",
	"*masterLabel*fontStyle: Italic",
	"*minLabel*fontStyle: Italic",
	
	/* legacy fonts for old desktop envs */
/*
	"*font: -adobe-helvetica-medium-r-*-*-14-*",
	"*cpuModel*font: -adobe-helvetica-bold-o-*-*-14-*",
	"*masterLabel*font: -adobe-helvetica-medium-o-*-*-14-*",
	"*minLabel*font: -adobe-helvetica-medium-o-*-*-14-*",
	"*usageLabel*font: -adobe-helvetica-bold-r-*-*-14-*",
	"*aboutLabel*font: -adobe-helvetica-bold-r-*-*-14-*",
*/
	NULL,
};

int main(int argc, char *argv[])
{

/* do we have root access? */
	verifySuperUser();
	
/* define Xt context */
	XtAppContext app;

/* open top level */
	Widget topLevel = XtVaAppInitialize(&app, "dvcpufreq", NULL, 0, &argc, argv, fbxres, NULL);
	XtVaSetValues(topLevel,
		XmNtitle, "CPU Frequency Scaling",
		XmNiconName, " CPU Frequency Scaling ",
	NULL);
	
	/* tell vendorshell (toplevel) to not use the maximize button */
	int decor;
	XtVaGetValues(topLevel, XmNmwmDecorations, &decor, NULL);
		decor &= ~MWM_DECOR_BORDER;
		decor &= ~MWM_DECOR_TITLE;
		decor &= ~MWM_DECOR_MENU;
		decor &= ~MWM_DECOR_MINIMIZE;
	XtVaSetValues(topLevel, XmNmwmDecorations, decor, NULL);
	
	/* this will make the min/max functions disappear while keeping move/close/min */
	int func;
	XtVaGetValues(topLevel, XmNmwmFunctions, &func, NULL);
		func &= ~MWM_FUNC_CLOSE;
		func &= ~MWM_FUNC_MOVE;
		func &= ~MWM_FUNC_MINIMIZE;
	XtVaSetValues(topLevel, XmNmwmFunctions, func, NULL);


/* main window as a manager widget */
	Widget mainWin = XtVaCreateManagedWidget("mainWin", xmFormWidgetClass, topLevel, NULL);
	XtVaSetValues(mainWin,
		XmNshadowThickness, 0,
	NULL);

/* create menu bar */
	Widget menuBar = XmCreateMenuBar(mainWin, "menuBar", NULL, 0);
	XtVaSetValues(menuBar,
		XmNshadowThickness, 1,
		XmNleftAttachment, XmATTACH_FORM,
		XmNrightAttachment, XmATTACH_FORM,
	NULL);
	
	/*
	Widget menuBar = XtVaCreateManagedWidget("menuBar", xmRowColumnWidgetClass, mainWin, 
		XmNrowColumnType, XmMENU_BAR,
		XmNshadowThickness, 1,
		XmNleftAttachment, XmATTACH_FORM,
		XmNrightAttachment, XmATTACH_FORM,
	NULL);
	*/

/* mini cpu icon */
	Pixmap cpu_pixmap = XCreateBitmapFromData(XtDisplay(topLevel),
	RootWindowOfScreen(XtScreen(topLevel)), cpu_bits, cpu_width, cpu_height);

/* get the CPU model name */
	/* list cpus command piped to grep to get model name line, then cut/awk/tr are used to remove unwanted characters */
	const char* get_cpu_name = "lscpu | grep -i -m 1 'model name' | cut -d : -f 2 | awk '{print $1, $3, $5, $6}' | tr -d '\n'";
	char* cpu_name = fetchOutputFromCmd(get_cpu_name);

/* mini cpu label */
	XmString cpuModelLabelString = XmStringCreateLocalized(cpu_name);
	Widget cpuIconLabel = XtVaCreateManagedWidget("cpuIconLabel", xmCascadeButtonWidgetClass, menuBar,
		XmNshadowThickness, 0,
		XmNwidth, 26,
		XmNheight, 26,
		XmNlabelType, XmPIXMAP_AND_STRING,
		XmNlabelPixmap, cpu_pixmap,
		XmNpixmapTextPadding, 8,
		XmNlabelString, cpuModelLabelString,
	NULL);
	XmStringFree(cpuModelLabelString);
	free(cpu_name);


/* create help menu */
	Widget helpMenu = XtVaCreateManagedWidget("Help", xmCascadeButtonWidgetClass, menuBar, NULL);    
	XtVaSetValues(helpMenu,
		XmNmnemonic, 'H',
		XmNshadowThickness, 0,
	NULL);

	/* let the menu bar know we want help on the right */
	XtVaSetValues(menuBar,
		XmNmenuHelpWidget, helpMenu,
	NULL);
	
	/* create the help pull down pane */
	Widget helpMenuPane = XmCreatePulldownMenu(helpMenu, "fileMenuPane", NULL, 0);
	XtVaSetValues(helpMenu,
		XmNsubMenuId, helpMenuPane,
		XmNshadowThickness, 0,
	NULL);
	
	/* create the usage button */
	Widget helpUsage = XtCreateManagedWidget("Usage", xmCascadeButtonWidgetClass, helpMenuPane, NULL, 0);
	XtVaSetValues(helpUsage,
		XmNmnemonic, 'U',
		XmNmarginWidth, 8,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
	NULL);
	XtAddCallback(helpUsage, XmNactivateCallback, usage_popup, NULL);
	
	/* create the about button */
	Widget helpAbout = XtCreateManagedWidget("About", xmCascadeButtonWidgetClass, helpMenuPane, NULL, 0);
	XtVaSetValues(helpAbout,
		XmNmnemonic, 'A',
		XmNmarginWidth, 8,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
	NULL);
	XtAddCallback(helpAbout, XmNactivateCallback, about_popup, NULL);
	
	XtManageChild(menuBar);


/* create main form below menu bar */
	Widget mainForm = XtVaCreateManagedWidget("mainForm", xmFormWidgetClass, mainWin, NULL);
	XtVaSetValues(mainForm,
		XmNshadowThickness, 1,
		XmNshadowType, XmSHADOW_IN,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, menuBar,
		XmNleftAttachment, XmATTACH_FORM,
		XmNrightAttachment, XmATTACH_FORM,
		XmNbottomAttachment, XmATTACH_FORM,
		
	NULL);

/* get the maximum CPU frequency */
	const char* get_cpu_max_freq = "echo \"$(cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq) / 1000\" | bc";
	int cpu_max_freq = fetchNumberFromCmd(get_cpu_max_freq);
	
/* get the minimum CPU frequency */
	const char* get_cpu_min_freq = "echo \"$(cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq) / 1000\" | bc";
	int cpu_min_freq = fetchNumberFromCmd(get_cpu_min_freq);
	
/* get the current scaling maximum */
	const char* get_cpu_max_scale = "echo \"$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq) / 1000\" | bc";
	int cpu_max_scale = fetchNumberFromCmd(get_cpu_max_scale);
	
/* get the current scaling minimum */
	const char* get_cpu_min_scale = "echo \"$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq) / 1000\" | bc";
	int cpu_min_scale = fetchNumberFromCmd(get_cpu_min_scale);

/* create master slider */
	scale1 = XmCreateScale(mainForm, "scale1", NULL, 0);
	XtVaSetValues(scale1,
		XmNorientation, XmHORIZONTAL,
		XmNminimum, cpu_min_freq,
		XmNmaximum, cpu_max_freq,
		XmNvalue, cpu_max_scale,
		XmNshowValue, True,
		XmNscaleMultiple, 100,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 8,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 6,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 6,
	NULL);	
	XtManageChild(scale1);
	
	/* callback for scale1 */
	XtAddCallback(scale1, XmNvalueChangedCallback, scale1_callback, NULL);

/* create master label */
	Widget masterLabel = XmCreateLabel(mainForm, "masterLabel", NULL, 0);
	XmString masterLabelString = XmStringCreateLocalized("Maximum (MHz / All Cores)");
	XtVaSetValues(masterLabel,
		XmNlabelString, masterLabelString,
		XmNalignment, XmALIGNMENT_BEGINNING,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, scale1,
		XmNtopOffset, 4,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 8,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 2,
	NULL);
	XmStringFree(masterLabelString);
	XtManageChild(masterLabel);


	Widget sep = XtVaCreateManagedWidget("sep", xmSeparatorWidgetClass, mainForm,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, masterLabel,
		XmNtopOffset, 4,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 1,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 1,
	NULL);

/* create second slider */
	scale2 = XmCreateScale(mainForm, "scale2", NULL, 0);
	XtVaSetValues(scale2,
		XmNorientation, XmHORIZONTAL,
		XmNminimum, cpu_min_freq,
		XmNmaximum, cpu_max_freq,
		XmNvalue, cpu_min_scale,
		XmNshowValue, True,
		XmNscaleMultiple, 100,
		
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, sep,
		XmNtopOffset, 7,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 6,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 6,
	NULL);	
	XtManageChild(scale2);
	
	/* callback for scale2 */
	XtAddCallback(scale2, XmNvalueChangedCallback, scale2_callback, NULL);



/* create master label */
	Widget minLabel = XmCreateLabel(mainForm, "minLabel", NULL, 0);
	XmString minLabelString = XmStringCreateLocalized("Minimum (MHz / All Cores)");
	XtVaSetValues(minLabel,
		XmNlabelString, minLabelString,
		XmNalignment, XmALIGNMENT_BEGINNING,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, scale2,
		XmNtopOffset, 4,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 8,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 2,
	NULL);
	XmStringFree(minLabelString);
	XtManageChild(minLabel);

/* decorative separator */
	Widget sep2 = XtVaCreateManagedWidget("sep2", xmSeparatorWidgetClass, mainForm,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, minLabel,
		XmNtopOffset, 4,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 1,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 1,
	NULL);

/* create radio box for governor toggle */
	Widget radioBox = XmCreateRadioBox(mainForm, "radioBox", NULL, 0);
	XtVaSetValues(radioBox,
		XmNorientation, XmHORIZONTAL,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, sep2,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 8,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 1,
	NULL);

	/* toggle for max performance */
	Widget radio1 = XmCreateToggleButton(radioBox, "Performance ", NULL, 0);
	XtVaSetValues(radio1,
		XmNshadowThickness, 0,
		XmNhighlightThickness, 0,
		XmNindicatorType, XmONE_OF_MANY_DIAMOND,
		XmNset, 0,
		XmNmarginTop, 0,
		XmNmarginBottom, 0,
		XmNspacing, 6,
	NULL);
	XtManageChild(radio1);
	
	XtAddCallback(radio1, XmNvalueChangedCallback, radio1_callback, NULL);

	/* toggle for power saving */
	Widget radio2 = XmCreateToggleButton(radioBox, "Powersave", NULL, 0);
	XtVaSetValues(radio2,
		XmNshadowThickness, 0,
		XmNhighlightThickness, 0,
		XmNindicatorType, XmONE_OF_MANY_DIAMOND,
		XmNset, 0,
		XmNmarginTop, 0,
		XmNmarginBottom, 0,
		XmNspacing, 6,
	NULL);
	XtManageChild(radio2);
	
	XtAddCallback(radio2, XmNvalueChangedCallback, radio2_callback, NULL);
	
	/* close radio box */
	XtManageChild(radioBox);
	
/* check performance governor's initial state and set radio toggles based on the result */
	int init_gov_state = isPerformanceModeEnabled();
	
	/* if performance mode is enabled */
	if(init_gov_state == 1)
	{
		XmToggleButtonSetState(radio1, True, False); /* XmToggleButtonSetState(widget, boolean, no notify) */
		XmToggleButtonSetState(radio2, False, False);
	}
	
	/* else if powersave is enabled */
	else if(init_gov_state == 0)
	{
		XmToggleButtonSetState(radio1, False, False);
		XmToggleButtonSetState(radio2, True, False);
	}
	
	/* else if result is not 1 or 0 we throw an error */
	else
	{
		/* print to standard error first */
		fprintf(stderr, "\ndvcpufreq: Could not retrieve performance governor from /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor \n\n");
		fflush(stderr);
		
		/* run graphical notifier window */
		system("dvnotifier -e -c -m \"dvcpufreq: Could not retrieve performance governor!\n(/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor) \n\nThis program is meant to interface with real hardware from\nthe host OS and requires Linux CPUFreq subsystem support.\nAdvanced power management features may not be available\non all platforms (i.e. virtual machines). \" &");
		
		/* exit immediately since there was a big problem */
		exit(0);
	}
	
	/*
	
	Pixmap reset_pixmap, reset_mask;
	XpmCreatePixmapFromData(XtDisplay(topLevel), RootWindowOfScreen(XtScreen(topLevel)),
	reset_xpm, &reset_pixmap, &reset_mask, NULL);
	
	*/
	
/* create reset icon */
	Pixmap reset_pixmap = XCreateBitmapFromData(XtDisplay(topLevel),
	RootWindowOfScreen(XtScreen(topLevel)), reset_bits, reset_width, reset_height);
	
/* create reset button */
	Widget resetButton = XmCreatePushButton(mainForm, "resetButton", NULL, 0);
	XtVaSetValues(resetButton,
		XmNhighlightThickness, 0,
		XmNshadowThickness, 2,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, sep2,
		XmNtopOffset, 4,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 6,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 6,
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, radioBox,
		XmNleftOffset, 40,
		XmNlabelType, XmPIXMAP_AND_STRING,
		XmNpixmapPlacement, XmPIXMAP_RIGHT,
		XmNpixmapTextPadding, 8,
		XmNmarginLeft, 9,
		XmNmarginRight, 8,
		XmNarmPixmap, reset_pixmap,
		XtVaTypedArg, XmNlabelString, XmRString, "Reset", 4,
		XmNheight, 36,
	NULL);
	XtManageChild(resetButton);
	
	/* when reset button is pushed reset master scale */
	XtAddCallback(resetButton, XmNactivateCallback, reset_scale1_callback, scale1);
	XtAddCallback(resetButton, XmNactivateCallback, reset_scale2_callback, scale2);
	

	/* close top level */
	XtRealizeWidget(topLevel);
	

/* center the window */
	Display *display = XtDisplay(topLevel);
	Window window = XtWindow(topLevel);
	
	/* check for xinerama extension info */
	int event_base, error_base;
	
	if(!XineramaQueryExtension(display, &event_base, &error_base)) 
	{
		fprintf(stderr, "ScreenSnap: Could not query Xinerama extensions.\n");
		fflush(stderr);
		XtAppMainLoop(app);
		return 1;
	}
	
	/* get screen count */
	int screen_count;
	XineramaScreenInfo *screen_info = XineramaQueryScreens(display, &screen_count);
	
	if(!screen_info || screen_count == 0)
	{
		fprintf(stderr, "ScreenSnap: Could not retrieve Xinerama screen info.\n");
		fflush(stderr);
		XtAppMainLoop(app);
		return 1;
	}
	
	/* get window attributes like height and width */
	XWindowAttributes windowAttr;
	XGetWindowAttributes(display, window, &windowAttr);
	int windowWidth = windowAttr.width;
	int windowHeight = windowAttr.height;
	
	if(screen_count > 0)
	{
		RootWindow(display, screen_info[0].screen_number);
		
		/* take height/width of window, add screen H/W, then divide by two */
		int newX = screen_info[0].x_org + (screen_info[0].width - windowWidth) / 2;
		int newY = screen_info[0].y_org + (screen_info[0].height - windowHeight) / 2;
		
		/* finally move window to center */
		XMoveWindow(display, window, newX, newY);
	}
	
	XFree(screen_info);
	
	
/* enter main processing loop */
	XtAppMainLoop(app);
}





