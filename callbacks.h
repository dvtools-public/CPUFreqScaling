
/* declared up here to make accessible to cb functions */
Widget scale1, scale2;

/* ensure root access is available or else freq setting is dead in the water */
void verifySuperUser()
{
	/* if UID is not zero... */
	if(getuid() != 0)
	{
		/* print to standard error first */
		fprintf(stderr, "\ndvcpufreq -> verifySuperUser: must be run as UID 0 (sudo or root).\n\n");
		fflush(stderr);
		
		/* then see if graphical notifier tool is installed */
		const char *binTest = "/usr/local/bin/dvnotifier";
		
		/* if dvnotifier binary exists... */
		if(access(binTest, X_OK) == 0)
		{
			/* run popup window */
			system("dvnotifier -e -c -m \"dvcpufreq -> verifySuperUser: must be run as UID 0 (sudo or root).\" &");
		}
		
		/* quits program */
		exit(0);
	}
}



void about_popup(Widget widget, XtPointer clientData, XtPointer callData)
{
	/* initialize a bare dialog shell */
	Widget aboutDialog = XmCreateDialogShell(widget, "aboutDialog", NULL, 0);
	/* set values like title and geometry for the popup window */
	XtVaSetValues(aboutDialog,
		XmNtitle, "About",
		XmNminWidth, 260,
		XmNminHeight, 140,
	NULL);
	
	/* tell the vendorshell we only want the mwm menu button and outer (non-resize) border */
	int decor;
	XtVaGetValues(aboutDialog, XmNmwmDecorations, &decor, NULL);
		decor &= ~MWM_DECOR_MENU;
		decor &= ~MWM_DECOR_BORDER;
	XtVaSetValues(aboutDialog, XmNmwmDecorations, decor, NULL);
	
/*	
	===================================================================================
	||                                                                               ||
	||   Normally mwm functions go here but it seems that dialog shells are more     ||
	||   restricted by default. or maybe the dialogs are picking up XmNmwmFunctions  ||
	||   already applied to the topLevel shell.                                      ||
	||                                                                               ||
	===================================================================================
*/
	
	/* create a form to hold the other widgets */
	Widget aboutDialogForm = XtVaCreateManagedWidget("aboutDialogForm", xmFormWidgetClass, aboutDialog,
		XmNshadowThickness, 1,
	NULL);
	
/* spark cpu icon */
	/*
	Pixmap dvcpufreq_pixmap, dvcpufreq_mask;
	XpmCreatePixmapFromData(XtDisplay(aboutDialog), RootWindowOfScreen(XtScreen(aboutDialog)),
	dvcpufreq_xpm, &dvcpufreq_pixmap, &dvcpufreq_mask, NULL);
	*/
	
/*
	Shape masks don't work. Code compiles but icon gets set to the bg color. Symbolic refs for the 
	main pixmap bg not working either.
*/

	/*
	Pixmap shapeMask, dvcpufreq_pixmap, mask;
	shapeMask = XCreateBitmapFromData(XtDisplay(aboutDialog), RootWindowOfScreen(XtScreen(aboutDialog)), 
	dvcpufreq_shapemask_bits, dvcpufreq_shapemask_width, dvcpufreq_shapemask_height);
	
	dvcpufreq_pixmap = XpmCreatePixmapFromData(XtDisplay(aboutDialog), RootWindowOfScreen(XtScreen(aboutDialog)),
	dvcpufreq_xpm, &dvcpufreq_pixmap, &shapeMask, NULL);
	*/
	

/* spark cpu label */
	/*
	Widget iconFrame = XtVaCreateManagedWidget("iconFrame", xmLabelWidgetClass, aboutDialogForm,
		XmNshadowThickness, 0,
		XmNtopAttachment, XmATTACH_FORM,
		XmNleftAttachment, XmATTACH_FORM,
		XmNlabelType, XmPIXMAP,
		XmNlabelPixmap, dvcpufreq_pixmap,
		XmNwidth, 48,
		XmNheight, 48,
	NULL);
	*/
		
	
/* etched frame around main message */
	Widget aboutLabelFrame = XtVaCreateManagedWidget("aboutLabelFrame", xmFrameWidgetClass, aboutDialogForm,
		XmNshadowThickness, 2,
		XmNtopAttachment, XmATTACH_FORM,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNrightAttachment, XmATTACH_FORM,
		XmNleftAttachment, XmATTACH_FORM,
		/* XmNleftWidget, iconFrame, */
		XmNtopOffset, 10,
		XmNbottomOffset, 52,
		XmNrightOffset, 10,
		XmNleftOffset, 10,
		XmNshadowType, XmSHADOW_ETCHED_IN,
	NULL);
	
/* main label widget */
	Widget aboutLabel = XmCreateLabel(aboutLabelFrame, "aboutLabel", NULL, 0);
	XmString aboutLabelString = XmStringCreateLocalized("CPU Freq. Scaling Tool v1.2\nMay 4th, 2024\nBSD Zero Clause");
	XtVaSetValues(aboutLabel,
		XmNlabelString, aboutLabelString,
		XmNalignment, XmALIGNMENT_BEGINNING,
		XmNmarginLeft, 8,
		XmNmarginRight, 8,
		XmNmarginTop, 8,
		XmNmarginBottom, 8,
	NULL);
	XmStringFree(aboutLabelString);
	XtManageChild(aboutLabel);
	
/* cancel the quit dialog */
	Widget aboutCancel = XtVaCreateManagedWidget("Dismiss", xmPushButtonWidgetClass, aboutDialogForm,
		XmNwidth, 80,
		XmNheight, 36,
		XmNrightAttachment, XmATTACH_FORM,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNrightOffset, 10,
		XmNbottomOffset, 10,
	NULL);
	
	/* add sound callback for the push button */
	/* XtAddCallback(aboutCancel, XmNactivateCallback, close_snd, NULL); */

	
	/* close the dialog shell */
	XtRealizeWidget(aboutDialog);
}


void usage_popup(Widget button, XtPointer clientData, XtPointer callData)
{
	/* initialize a bare dialog shell */
	Widget usageDialog = XmCreateDialogShell(button, "usageDialog", NULL, 0);
	/* set values like title and geometry for the popup window */
	XtVaSetValues(usageDialog,
		XmNtitle, "Usage",
		XmNminWidth, 340,
		XmNminHeight, 210,
	NULL);
	
	/* tell the vendorshell we only want the mwm menu button and outer (non-resize) border */
	int decor;
	XtVaGetValues(usageDialog, XmNmwmDecorations, &decor, NULL);
		decor &= ~MWM_DECOR_MENU;
		decor &= ~MWM_DECOR_BORDER;
	XtVaSetValues(usageDialog, XmNmwmDecorations, decor, NULL);
	

	/* create a form to hold the other widgets */
	Widget usageDialogForm = XtVaCreateManagedWidget("usageDialogForm", xmFormWidgetClass, usageDialog,
		XmNshadowThickness, 1,
	NULL);
	
	/* etched frame around main message */
	Widget usageLabelFrame = XtVaCreateManagedWidget("usageLabelFrame", xmFrameWidgetClass, usageDialogForm,
		XmNshadowThickness, 2,
		XmNtopAttachment, XmATTACH_FORM,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNrightAttachment, XmATTACH_FORM,
		XmNleftAttachment, XmATTACH_FORM,
		XmNtopOffset, 10,
		XmNbottomOffset, 52,
		XmNrightOffset, 10,
		XmNleftOffset, 10,
		XmNshadowType, XmSHADOW_ETCHED_IN,
	NULL);
	
	Widget usageLabel = XmCreateLabel(usageLabelFrame, "usageLabel", NULL, 0);
	XmString usageLabelString = XmStringCreateLocalized("This has only been tested on Intel 8th gen.\nIt might not work on other platforms such\nas Intel 12th gen. or newer processors.\n\nUse the sliders to adjust CPU frequencies\non all cores. Reset button will not touch\nthe performance governor toggles.");
	XtVaSetValues(usageLabel,
		XmNlabelString, usageLabelString,
		XmNalignment, XmALIGNMENT_BEGINNING,
		XmNmarginLeft, 8,
		XmNmarginRight, 8,
		XmNmarginTop, 8,
		XmNmarginBottom, 8,
	NULL);
	XmStringFree(usageLabelString);
	XtManageChild(usageLabel);
	
	/* cancel the quit dialog */
	Widget usageCancel = XtVaCreateManagedWidget("Dismiss", xmPushButtonWidgetClass, usageDialogForm,
		XmNwidth, 80,
		XmNheight, 36,
		XmNrightAttachment, XmATTACH_FORM,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNrightOffset, 10,
		XmNbottomOffset, 10,
	NULL);
	
	/* close the dialog shell */
	XtRealizeWidget(usageDialog);
}


char* fetchOutputFromCmd(const char* command)
{
	char* output = NULL;
	char cmdBuffer[4096];
	FILE* pipe;

	pipe = popen(command, "r");
	if(!pipe) /* no pipe */
	{
		fprintf(stderr, "dvcpufreq: fetchOutputFromCmd: Could not open file pipe.\n");
		fflush(stderr);
		return NULL;
	}

	/* read cmd output into buffer */
	size_t outputSize = 0;
	while(fgets(cmdBuffer, sizeof(cmdBuffer), pipe) != NULL) 
	{
		size_t len = strlen(cmdBuffer);
		output = realloc(output, outputSize + len + 1);
		
		if(!output) /* if no command output */
		{
			fprintf(stderr, "dvcpufreq: fetchOutputFromCmd: Could not get output.\n");
			fflush(stderr);
			return NULL;
		}
		
		strcpy(output + outputSize, cmdBuffer);
		outputSize += len;
	}

	/* close pipe */
	pclose(pipe);

	/* terminate output string */
	output = realloc(output, outputSize + 1);
	if(!output) /* no output post termination */
	{
		fprintf(stderr, "dvcpufreq: fetchOutputFromCmd: No output found.\n");
		fflush(stderr);
		return NULL;
	}
	
	output[outputSize] = '\0';
	return output;
}


int fetchNumberFromCmd(const char* command)
{
	int number = 0;
	FILE* pipe;

	pipe = popen(command, "r");
	if(!pipe) /* no pipe */
	{
		fprintf(stderr, "dvcpufreq: fetchNumberFromCmd: Could not open file pipe.\n");
		fflush(stderr);
		return number;
	}

	if(fscanf(pipe, "%d", &number) != 1) /* nothing in pipe to smoke */
	{
		fprintf(stderr, "dvcpufreq: fetchNumberFromCmd: Could not get number.\n");
		fflush(stderr);
		number = 0;
	}

	pclose(pipe);
	return number;
}


void scale1_callback(Widget widget, XtPointer client_data, XtPointer call_data)
{
	/* repaint widget with every scale movement */
	XmUpdateDisplay(scale1);
	
	/* grab number from scale widget */
	int scale1Value;
	XtVaGetValues(scale1, XmNvalue, &scale1Value, NULL);
	
	char cmd[50];
	/* roundhouse kick obnoxious outputs, slam dunk obnoxious outputs into the null device */
	sprintf(cmd, "cpupower frequency-set --max %dMHz > /dev/null 2>&1", scale1Value);
	system(cmd);
}


void scale2_callback(Widget widget, XtPointer client_data, XtPointer call_data)
{
	/* repaint widget with every scale movement */
	XmUpdateDisplay(scale2);
	
	/* grab number from scale widget */
	int scale2Value;
	XtVaGetValues(scale2, XmNvalue, &scale2Value, NULL);
	
	char cmd[50];
	/* roundhouse kick obnoxious outputs, slam dunk obnoxious outputs into the null device */
	sprintf(cmd, "cpupower frequency-set --min %dMHz > /dev/null 2>&1", scale2Value);
	system(cmd);
}




void radio1_callback(Widget widget, XtPointer client_data, XtPointer call_data)
{
	XmToggleButtonCallbackStruct *cbs = (XmToggleButtonCallbackStruct *)call_data;
	if(cbs->set) { system("cpupower frequency-set --governor performance"); }
}

void radio2_callback(Widget widget, XtPointer client_data, XtPointer call_data)
{
	XmToggleButtonCallbackStruct *cbs = (XmToggleButtonCallbackStruct *)call_data;
	if(cbs->set) { system("sudo cpupower frequency-set --governor powersave"); }
}


/* check to find active scaling governor */
int isPerformanceModeEnabled()
{
	FILE* file = fopen("/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor", "r");
	if(!file) /* if file not found */
	{
		fprintf(stderr, "dvcpufreq: isPerformanceModeEnabled: Could not open scaling_governor file.\n");
		fflush(stderr);
		return -1;
	}

	char governor[32];
	if(fgets(governor, sizeof(governor), file) == NULL)
	{
		fprintf(stderr, "dvcpufreq: isPerformanceModeEnabled: Could not read contents of scaling_governor file.\n");
		fflush(stderr);
		fclose(file); /* close scaling_governor file */
		return -1;
	}
	
	/* close scaling_gov file */
	fclose(file);
	
	size_t len = strlen(governor);
	if(len > 0 && governor[len - 1] == '\n') { governor[len - 1] = '\0'; }
	
	if(strcmp(governor, "performance") == 0) { return 1; } 
	
	else { return 0; }
}



/* callback to reset some stuff to defaults */
void reset_scale1_callback(Widget widget, XtPointer client_data, XtPointer call_data)
{

/* get the maximum CPU frequency */
	const char* get_cpu_max_freq = "echo \"$(cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq) / 1000\" | bc";
	const int cpu_max_freq = fetchNumberFromCmd(get_cpu_max_freq);
	
	Widget scale1 = (Widget)client_data;
	XmScaleSetValue(scale1, cpu_max_freq);
	
	/* force immediate scale repaint */
	XmUpdateDisplay(scale1);
	
	char cmd[100];
	sprintf(cmd, "cpupower frequency-set --max %dMHz", cpu_max_freq);
	int result = system(cmd);
}

void reset_scale2_callback(Widget widget, XtPointer client_data, XtPointer call_data)
{

/* get the minimum CPU frequency */
	const char* get_cpu_min_freq = "echo \"$(cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq) / 1000\" | bc";
	int cpu_min_freq = fetchNumberFromCmd(get_cpu_min_freq);
	
	Widget scale2 = (Widget)client_data;
	XmScaleSetValue(scale2, cpu_min_freq);
	XmUpdateDisplay(scale2);
	
	char cmd[100];
	sprintf(cmd, "cpupower frequency-set --min %dMHz", cpu_min_freq);
	int result = system(cmd);
}


