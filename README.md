### CPU Frequency Scaling

There is currently no code to detect heterogeneous core configurations. I have no idea how this works on Intel chips with e-cores or ARM big.LITTLE. It could only set frequencies for half the cores or fail to work at all. This is for Linux only and has been tested with Intel 8th gen.

It must be run as UID 0 (sudo or root) or it will exit.

![screenshot](screenshots/dvcpufreq_sample.png)

#### Features

- Set min/max frequencies on all cores in MHz.
- Set performance governor.
- Shows CPU model and base frequency.

#### Requirements

- Motif 2.3.8
- X11/Xlib
- libXpm
- libXinerama
- cpupower
- dvnotifier (optional)

#### Build Instructions

Compile:

```
make clean && make
```

Run:

```
sudo ./dvcpufreq
```

Install:

```
sudo make install
```

Uninstall:

```
sudo make uninstall
```

#### License

This software is distributed free of charge under the BSD Zero Clause license.